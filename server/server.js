require( './config/config' )
const express = require( 'express' );
const app = express();
const bodyParser = require( 'body-parser' );
const path = require( 'path' );

app.use( bodyParser.urlencoded( { extended: false } ) );
app.use( bodyParser.json() );
app.set( 'view engine', 'jade' )

app.use(express.static('public'));
app.use(express.static('node_modules'));


// configuracion global de rutas
app.use( require( './routes/routes.js' ) );

app.listen( process.env.PORT, () => {
    console.log( 'Servidor corriendo en el puerto: ' + process.env.PORT );
} );
