// ======================================
// Puerto
// ======================================
process.env.PORT = process.env.PORT || 3000;

// ======================================
// Entorno
// ======================================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ======================================
// Vencimiento del token
// ======================================
process.env.CADUCIDAD_TOKEN = '48h';

// ======================================
// Semilla de autencicacion SEED
// ======================================
process.env.SEED_TOKEN = process.env.SEED_TOKEN || 'secret-desarrollo';

// ======================================
// Base de datos        //urlDB = 'mongodb://root:abc123@ds261460.mlab.com:61460/prueba10';
// ======================================
let urlDB;
if( process.env.NODE_ENV === 'dev' )
    urlDB = 'mongodb://localhost:27017/cafe';
else
    urlDB = process.env.MONGO_URI;
process.env.URL_DB = urlDB;

// ======================================
// Google Client ID
// ======================================
process.env.CLIENT_ID = process.env.CLIENT_ID || '941300545925-1aod454mrbiem996o7c1991130usslbh.apps.googleusercontent.com';
